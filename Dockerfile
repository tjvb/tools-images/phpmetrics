FROM php:8.0-alpine as installer

ARG PHPMETRICSVERSION=*

COPY --from=composer /usr/bin/composer /usr/bin/composer

RUN mkdir -p /tmp/phpmetrics && \
    cd /tmp/phpmetrics && \
	/usr/bin/composer require phpmetrics/phpmetrics:${PHPMETRICSVERSION}

FROM php:8.0-alpine

RUN mkdir -p /tools
COPY --from=installer /tmp/phpmetrics /tools/phpmetrics
RUN ln -s  /tools/phpmetrics/vendor/bin/phpmetrics /usr/bin/phpmetrics